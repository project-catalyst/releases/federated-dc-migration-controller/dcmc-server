"""DCMC Server URL Configuration."""
from django.conf.urls import url, include
from django.contrib import admin

from api.urls import api_urlpatterns

prefix = 'catalyst/dcmcs'
urlpatterns = [
    url(r'^{}/admin/'.format(prefix), admin.site.urls),
    url(r'^{}/api/'.format(prefix), include(api_urlpatterns, namespace='api')),

    # Sphinx Documentation
    url(r'^{}/code-docs/'.format(prefix), include('docs.urls')),
]
