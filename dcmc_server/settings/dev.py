"""Development settings for dcmc_server project."""

from .base import *

# ==================================
#             DEBUGGING
# ==================================
DEBUG = True

# ==================================
#           ALLOWED HOSTS
# ==================================
ALLOWED_HOSTS = ['*']

# ==================================
#          DCMC MASTER HOST
# ==================================
DCMCS_HOST = {
    'PROTOCOL': 'http',
    'IP': '127.0.0.1',
    'PORT': 8000
}

# ==================================
#       RDBMS CONFIGURATION
# ==================================
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'dcmc_server',
        'USER': 'postgres',
        'PASSWORD': '12345678',
        'HOST': '127.0.0.1',
        'PORT': 5432
    }
}

# ==================================
#          REDIS SETTINGS
# ==================================
REDIS_HOST = {
    'IP': '127.0.0.1',
    'PORT': 6379
}

# =================================
#          CELERY SETTINGS
# =================================
BROKER_URL = 'redis://{}:{}/0'.format(REDIS_HOST['IP'], REDIS_HOST['PORT'])
CELERY_RESULT_BACKEND = BROKER_URL
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'
CELERY_ALWAYS_EAGER = False
