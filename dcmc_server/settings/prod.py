"""Production settings for dcmc_server project."""

from .base import *

# ==================================
#             DEBUGGING
# ==================================
DEBUG = False

# ==================================
#           ALLOWED HOSTS
# ==================================
ALLOWED_HOSTS = ['*']

# ==================================
#          DCMC MASTER HOST
# ==================================
DCMCS_HOST = {
    'PROTOCOL': os.getenv('DCMCS_HOST_PROTOCOL'),
    'IP': os.getenv('DCMCS_HOST_IP'),
    'PORT': os.getenv('DCMCS_HOST_PORT')
}

# =================================
#       RDBMS CONFIGURATION
# =================================
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('DCMCS_DB_NAME'),
        'USER': os.getenv('DCMCS_DB_USER'),
        'PASSWORD': os.getenv('DCMCS_DB_PASSWORD'),
        'HOST': os.getenv('DCMCS_DB_HOST'),
        'PORT': os.getenv('DCMCS_DB_PORT')
    }
}

# ==================================
#          REDIS SETTINGS
# ==================================
REDIS_HOST = {
    'IP': os.getenv('DCMCS_REDIS_HOST'),
    'PORT': os.getenv('DCMCS_REDIS_PORT')
}

# =================================
#          CELERY SETTINGS
# =================================
BROKER_URL = 'redis://{}:{}/0'.format(REDIS_HOST['IP'], REDIS_HOST['PORT'])
CELERY_RESULT_BACKEND = BROKER_URL
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'
CELERY_ALWAYS_EAGER = False
