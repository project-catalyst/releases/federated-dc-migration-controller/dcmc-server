"""Base settings for DCMC Server project. """

import os

# =================================
#           PROJECT ROOT
# =================================
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(os.path.join(__file__, os.pardir))))

# ==================================
#      APPLICATIONS DEFINITION
# ==================================
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'api',
    'docs',
    'drf_yasg',
]

# ==================================
#       MIDDLEWARE MANAGEMENT
# ==================================
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# =================================
#          BASIC SETTINGS
# =================================
ROOT_URLCONF = 'dcmc_server.urls'
WSGI_APPLICATION = 'dcmc_server.wsgi.application'
SECRET_KEY = 'u$w&-u$b62iln8ic$los(objxx^ky^s^y9t46acb&ls0j+v$4w'

# =================================
# TIMEZONE SETTINGS
# =================================
TIME_ZONE = 'UTC'
USE_TZ = True

# =================================
# MULTILINGUAL SETTINGS
# =================================
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True

# ==================================
# TEMPLATES CONFIGURATION
# ==================================
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# =================================
# STATIC FILES SETTINGS
# =================================
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static').replace('\\', '/')
STATIC_URL = '/static/'

# ==================================
# LOGGING SETTINGS
# ==================================
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] - [%(name)s:%(lineno)s] - [%(levelname)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'api': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '{}/logs/api.log'.format(PROJECT_ROOT),
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'task_logger': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '{}/logs/celery-tasks.log'.format(PROJECT_ROOT),
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'WARN',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'api_logger': {
            'handlers': ['api'],
            'level': 'DEBUG',
        },
        'task_logger': {
            'handlers': ['task_logger'],
            'level': 'DEBUG',
        }
    }
}

# =================================
#         SWAGGER SETTINGS
# =================================
SWAGGER_SETTINGS = {
    'enabled_methods': [
        'get',
        'post',
        'put',
        'patch',
        'delete',
    ],
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'description': "",
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        },
    },
    'JSON_EDITOR': False,
}

# =================================
#         KEYCLOAK SETTINGS
# =================================
KEYCLOAK_HOST_URL = os.getenv('DCMCS_KEYCLOAK_URL')
KEYCLOAK_REALM = os.getenv('DCMCS_KEYCLOAK_REALM')
KEYCLOAK_CLIENT_ID = os.getenv('DCMCS_KEYCLOAK_CLIENT_ID')
KEYCLOAK_CLIENT_SECRET = os.getenv('DCMCS_KEYCLOAK_CLIENT_SECRET')

# =================================
#        SPHINX DOCS ROOT
# =================================
DOCS_ROOT = os.path.join(PROJECT_ROOT, 'docs/build/html')
