"""WSGI config for DCMC Server project. """

import sys

from django.core.wsgi import get_wsgi_application

sys.path.append('/opt/dcmc-server')
application = get_wsgi_application()
