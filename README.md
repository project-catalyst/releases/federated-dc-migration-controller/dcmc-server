# DC Migration Controller Server (DCMC Server)

This is the repository for the DCMC Server, implemented for the Migration Controller of the H2020 CATALYST Project. The 
DCMC Server lies in the Federation part of the CATALYST architecture and keeps track of the DCs in the federation, while 
being responsible for triggering the initiation and synchronization of the migration procedures.

## Installation Guide

### Prerequisites

For the deployment of the DCMC Server component the Docker engine as well as docker-compose should be installed.
These actions can be performed following the instructions provided below. Firstly, an update should be performed
and essential packages should be installed:

```bash
sudo apt-get update
sudo apt-get install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     software-properties-common
```

Secondly the key and Docker repository should be added:

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) \
     stable"
```

Then another update is performed, Docker is installed and the user is added to docker group.

```bash
sudo apt-get update
sudo apt-get install -y docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER
```

Finally, docker-compose should be installed:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### Environmental Parameters
The services that are executed as containers, thus forming the DCMC Server receive their configurations from a .env file 
located in the root of the repository. In the following tables, the environmental parameters that are necessary for the 
configuration and deployment of the DCMC Server are recorded and described.

__Docker and Internal Services__

| Parameter | Tag | Description |
| --------- | --- | ----------- |
| COMPOSE_PROJECT_NAME | `docker` | The name of the project (default: catalyst) |
| PG_IMAGE_TAG | `PostgreSQL` | The Postgresql Docker Image tag |
| PG_PORT | `PostgreSQL` | Port |
| PG_USER | `PostgreSQL` | User |
| PG_PASSWORD | `PostgreSQL` | Password |
| PG_DB | `PostgreSQL` | DB Name |
| REDIS_IMAGE_TAG | `Redis` | Docker Image tag |
| REDIS_PORT | `Redis` | Port |
| DCMCS_ENV | `DCMC Server` | Environment (dev / prod) |
| DCMCS_API_PORT | `DCMC Server` | API Port |
| DCMCS_SUPERVISOR_PORT | `DCMC Server` | Supervisor Port |
| DCMCS_DB_HOST | `DCMC Server` | DB Host |
| DCMCS_DB_PORT | `DCMC Server` | DB Port |
| DCMCS_DB_USER | `DCMC Server` | DB User |
| DCMCS_DB_PASSWORD | `DCMC Server` | DB Password |
| DCMCS_DB_NAME | `DCMC Server` | DB Name |
| DCMCS_REDIS_HOST | `DCMC Server` | Redis Host |
| DCMCS_REDIS_PORT | `DCMC Server` | Redis Port |
| DCMCS_HOST_IP | `DCMC Server` | Host IP |
| DCMCS_HOST_PORT | `DCMC Server` | Port |
| DCMCS_HOST_PROTOCOL | `DCMC Server` | Protocol |

__External services__

| Parameter | Tag | Description |
| --------- | --- | ----------- |
| DCMCS_KEYCLOAK_URL | `Keycloak` | Host URL |
| DCMCS_KEYCLOAK_REALM | `Keycloak` | Realm |
| DCMCS_KEYCLOAK_CLIENT_ID | `Keycloak` | Client ID |
| DCMCS_KEYCLOAK_CLIENT_SECRET | `Keycloak` | Client Secret |

### Deployment

The DCMC Server is deployed as collection of Docker containers, utilizing docker-compose. Having cloned the code of this 
repository, and having created the .env file the following commands should be executed:

```bash
cp .env /dcmc-server
cd dcmc-server
docker-compose up --build -d
```

## API
Here, the API that DCMC Server offers is briefly documented. For more information you may check both the Swagger
documentation and the DCMC Server Documentation, Release v1.0.0. The prefix of the API endpoints is /catalyst/dcmcs/api.

### Register Datacenter
| **URL**             |	/datacenter/register/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json <br>Authorization: Bearer DCMC_S_TOKEN        |
| **Request body**	  | [DatacenterRegister](#datacenterregister)                             |
| **Response body**	  | N/A                                        |
| **Response codes**  | 201 – Datacenter was registered.<br>400 – Provided data is invalid or malformed<br>401 – Unauthorized Datacenters cannot be registered.<br>403 – Operation forbidden for this type of user<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Report accepted migration
| **URL**             |	/migration/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json<br>Authorization: Bearer DCMC_S_TOKEN          |
| **Request body**	  | [Migration](#migration)                            |
| **Response body**	  | N/A                                        |
| **Response codes**  | 201 – Reported accepted migration. Migration object was created. <br>400 – Provided data is invalid or malformed <br>401 – 	Unauthorized users are not allowed to create resources <br>403 – Operation forbidden for this type of user<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Request token
| **URL**             |	/token/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json         |
| **Request body**	  | [Token](#token)                            |
| **Response body**	  | N/A                                        |
| **Response codes**  | 201 – Access Token was created<br>400 –Provided data is invalid or malformed<br>404 – The Keycloak Authorization URL was not found. No token was created.<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Send VC details
| **URL**             |	/vcontainer/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json<br>Authorization: Bearer DCMC_S_TOKEN          |
| **Request body**	  | [Vcontainer](#vcontainer)                            |
| **Response body**	  | N/A                                        |
| **Response codes**  | 201 – VC Creation was triggered. VC Object was created.<br>400 – Provided data is invalid or malformed<br>401 – Unauthorized users are not allowed to create resources<br> 403 – Operation forbidden for this type of user<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error<br>|

### Retrieve Controller Details
| **URL**             |	/transaction/{transaction_id}/controller/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json<br>Authorization: Bearer DCMC_S_TOKEN          |
| **Request body**	  | N/A                                        |
| **Response body**	  | [ControllerDetails](#controllerdetails)    |
| **Response codes**  | 200 – Controller details were retrieved.<br>400 – Provided data is invalid or malformed<br>401 – Unauthorized users are not allowed to create resources<br> 403 – Operation forbidden for this type of user<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error<br>|

### Rollback Migration in Destination
| **URL**             |	/transaction/{transaction_id}/rollback/destination |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json<br>Authorization: Bearer DCMC_S_TOKEN          |
| **Request body**	  | [Vcontainer](#vcontainer)                  |
| **Response body**	  | N/A                                        |
| **Response codes**  | 202 – Migration rollback was accepted <br>400 – Provided data is invalid or malformed<br>401 – Unauthorized users are not allowed to create resources<br> 403 – Operation forbidden for this type of user<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error<br>|


### Data Model

#### DatacenterRegister
```json
{
   "required": ["dc_name", "ip_address", "username", "password"], 
   "type": "object", 
   "properties": {
      "dc_name": {
         "title": "Dc name", 
         "description": "The registered name of the Datacenter in the federation", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }, 
      "ip_address": {
         "title": "Ip address", 
         "description": "The IP Address of the Datacenter under registration", 
         "type": "string", 
         "format": "uri", 
         "maxLength": 200, 
         "minLength": 1
      }, 
      "username": {
         "title": "Username", 
         "description": "The username for accessing the Datacenter's DCMC Master", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }, 
      "password": {
         "title": "Password", 
         "description": "The password for accessing the Datacenter's DCMC Master", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }
   }
}
```

#### Migration
```json
{
   "required": ["dc_name", "role", "delivery_start", "delivery_end", "transaction"], 
   "type": "object", 
   "properties": {
      "dc_name": {
         "title": "Dc name", 
         "description": "Username of the issuing DC as understood by the DCMC Server", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }, 
      "role": {
         "title": "Role", 
         "description": "The role of the issuing DC in the migration", 
         "type": "string", 
         "enum": ["source", "destination"]
      }, 
      "delivery_start": {
         "title": "Delivery start", 
         "description": "The time on which the migration is planned to start", 
         "type": "string", 
         "format": "date-time"
      }, 
      "delivery_end": {
         "title": "Delivery end", 
         "description": "The time on which the remote execution is planned to end", 
         "type": "string", 
         "format": "date-time"
      }, 
      "transaction": {
         "title": "Transaction", 
         "description": "The ID of the transaction as kept in the CATALYST IT Load Marketplace", 
         "type": "integer", 
         "minimum": 1
      }
   }
}
```

#### Token
```json
{
   "required": ["username", "password"], 
   "type": "object", 
   "properties": {
      "username": {
         "title": "Username", 
         "description": "Username of issuing DC for gaining authorization in the DCMC Server", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }, 
      "password": {
         "title": "Password", 
         "description": "Password of issuing DC for gaining authorization in the DCMC Server", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }
   }
}
```

#### VContainer
```json
{   
   "required": ["cpu", "cpu_uom", "ram", "ram_uom", "disk", "disk_uom", "start", "end", "transaction"], 
   "type": "object", 
   "properties": {
      "cpu": {
         "title": "Cpu", 
         "description": "The number of CPU cores of the VC described", 
         "type": "integer", 
         "minimum": 1
      }, 
      "cpu_uom": {
         "title": "Cpu uom", 
         "description": "The unit of measurement for the CPU value", 
         "type": "string", 
         "maxLength": 5, 
         "minLength": 1
      }, 
      "ram": {
         "title": "Ram", 
         "description": "The amount of RAM of the VC described", 
         "type": "integer", 
         "minimum": 1
      }, 
      "ram_uom": {
         "title": "Ram uom", 
         "description": "The unit of measurement for the RAM value", 
         "type": "string", 
         "maxLength": 5, 
         "minLength": 1
      }, 
      "disk": {
         "title": "Disk", 
         "description": "The amount of disk of the VC described", 
         "type": "integer", 
         "minimum": 1
      }, 
      "disk_uom": {
         "title": "Disk uom", 
         "description": "The unit of measurement for the disk value", 
         "type": "string", 
         "maxLength": 5, 
         "minLength": 1
      }, 
      "start": {
         "title": "Start", 
         "description": "The time from which the VC is planned to be active", 
         "type": "string", 
         "format": "date-time"
      }, 
      "end": {
         "title": "End", 
         "description": "The time until which the VC is planned to be active", 
         "type": "string", 
         "format": "date-time"
      }, 
      "transaction": {
         "title": "Transaction", 
         "description": "The ID of the transaction as kept in the CATALYST IT Load Marketplace", 
         "type": "integer", 
         "minimum": 1
      }
   }
}
```

#### ControllerDetails
```json
{
   "required": ["name", "ip", "port"], 
   "type": "object", 
   "properties": {
      "name": {
         "title": "Name", 
         "description": "The name of the Datacenter", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }, 
      "ip": {
         "title": "Ip", 
         "description": "The IP of the Datacenter", 
         "type": "string", 
         "maxLength": 15, 
         "minLength": 1
      },
      "port": {
         "title": "Port",
         "description": "The Port the DCMC Master listens on",
         "type": "number",
         "default": 60001
      }
   }
}
```

### Parameters
| **Parameter** | **Type** | **Comments** |
| ------------- | ------------- | ------------- |
| transaction_id | Integer | The id of the transaction, as kept in the CATALYST IT Load Marketplace. |