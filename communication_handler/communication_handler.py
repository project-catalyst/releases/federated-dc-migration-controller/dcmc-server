from django.conf import settings

from api.models import Datacenter
from communication_handler.api_clients.dcmcm_api import DCMCMasterClient
from communication_handler.api_clients.keycloak_api import KeycloakAPIClient

communication_handler = None


def get_communication_handler():
    """Get an Instance of the CommunicationHandler Class

    Returns
    -------
    communication_handler : CommunicationHandler
        An instance of the CommunicationHandler Class

    """
    global communication_handler
    if communication_handler is None:
        communication_handler = CommunicationHandler()
    return communication_handler


class CommunicationHandler(object):
    """Communication Handler Class.

    This is a wrapper class for access to all API clients that are necessary for the
    operation of the DCMC Server component. The API clients can be accessed through
    the properties of this class.

    """
    def __init__(self):
        """Communication Handler Constructor Class. """
        self.__dcmcm_client = {}
        self.__keycloak_client = None

    def dcmcm(self, dc_name):
        """DCMC Master Client.

        Parameters
        ----------
        dc_name : str
            The name of the Datacenter

        """
        if dc_name not in self.__dcmcm_client.keys():
            datacenter = Datacenter.objects.get(dc_name=dc_name)
            self.__dcmcm_client[dc_name] = DCMCMasterClient(dcmcm_host_url=datacenter.ip_address,
                                                            username=datacenter.username,
                                                            password=datacenter.password)
        return self.__dcmcm_client[dc_name]

    @property
    def keycloak(self):
        """Keycloak API Client. """
        if self.__keycloak_client is None:
            self.__keycloak_client = KeycloakAPIClient(keycloak_host_url=settings.KEYCLOAK_HOST_URL,
                                                       realm=settings.KEYCLOAK_REALM,
                                                       client_id=settings.KEYCLOAK_CLIENT_ID,
                                                       client_secret=settings.KEYCLOAK_CLIENT_SECRET)
        return self.__keycloak_client
