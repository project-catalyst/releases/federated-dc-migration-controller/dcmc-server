import requests
import urllib3
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

from .baseclient import AbstractClient

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class Client(AbstractClient):
    MAX_RETRIES = 5

    def __init__(self, verify_ssl_cert=False):
        self.verify_ssl_cert = verify_ssl_cert
        super(Client, self).__init__()

    @staticmethod
    def request_with_retrial(retries, session=None, backoff_factor=0.3, status_forcelist=(500, 502, 503, 504)):
        session = session or requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
            status_forcelist=status_forcelist,
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session

    def list(self, url, headers=None, **kwargs):
        """Fetch a list of entities (a collection).

        Args:
            url (str): the endpoint of the web service
            headers (dict): the required HTTP headers, e.g., Accept: application/json
            kwargs (dict, optional): Additional arguments will be passed to the request.

        Returns:
            obj: a requests object
        """
        if headers is None:
            headers = {}
        query_params = kwargs.get('query_params', None)
        request = self.request_with_retrial(retries=Client.MAX_RETRIES)
        response = request.get(url, headers=headers, params=query_params, verify=self.verify_ssl_cert)
        return response

    def get(self, url, headers=None, **kwargs):
        """Fetch an entity.

        Args:
            url (str): the endpoint of the web service
            headers (dict): the required HTTP headers, e.g., Accept: application/json
            kwargs (dict, optional): Additional arguments will be passed to the request.

        Returns:
            obj: a requests object
        """
        if headers is None:
            headers = {}
        query_params = kwargs.get('query_params', None)
        request = self.request_with_retrial(retries=Client.MAX_RETRIES)
        response = request.get(url, headers=headers, params=query_params, verify=self.verify_ssl_cert)
        return response

    def post(self, url, headers=None, payload=None, **kwargs):
        """Insert an entity.

        Args:
            url (str): the endpoint of the web service
            headers (dict): the required HTTP headers, e.g., Accept: application/json
            payload (dict, str): data that will be encoded as JSON and passed in the request
            kwargs (dict, optional): Additional arguments will be passed to the request.

        Returns:
            obj: a requests object
        """
        if headers is None:
            headers = {}
        query_params = kwargs.get('query_params', None)
        request = self.request_with_retrial(retries=Client.MAX_RETRIES)
        response = request.post(url, data=payload, headers=headers,
                                params=query_params, verify=self.verify_ssl_cert)
        return response

    def delete(self, url, headers=None, **kwargs):
        """Delete an entity.

        Args:
            url (str): the endpoint of the web service
            headers (dict): the required HTTP headers, e.g., Accept: application/json
            kwargs (dict, optional): Additional arguments will be passed to the request.

        Returns:
            obj: a requests object
        """
        if headers is None:
            headers = {}
        query_params = kwargs.get('query_params', None)
        request = self.request_with_retrial(retries=Client.MAX_RETRIES)
        response = request.delete(url=url, headers=headers, params=query_params, verify=self.verify_ssl_cert)
        return response
