from urllib.parse import urljoin

from .http_client.client import Client


class KeycloakAPIClient(object):
    """Keycloak API Client Class.

    Methods
    -------
    get_user_access_token(username, password)
        Retrieve a user access token.
    user_introspection(user_token)
        Introspect a user's authorization token.

    """
    __OPENID_TOKEN = 'realms/{realm-name}/protocol/openid-connect/token'
    __INTROSPECTION = 'realms/{realm-name}/protocol/openid-connect/token/introspect/'

    def __init__(self, keycloak_host_url, realm, client_id, client_secret):
        """Keycloak API Client Class Constructor.

        Parameters
        ----------
        keycloak_host_url : URL
            The Keycloak's host URL
        realm : str
            The name of the realm
        client_id : str
            The Keycloak's Client ID
        client_secret : str
            The Keycloak's Client Secret

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = keycloak_host_url
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        self.__headers_form = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        self.__realm = realm
        self.__client_id = client_id
        self.__client_secret = client_secret

    def get_user_access_token(self, username, password):
        """Retrieve an access token for a user

        Parameters
        ----------
        username : str
            The username of the user to get token for.
        password : str
            The password of the user to fetch token for.

        Returns
        -------
        Response
            The Response object of the issued request

        """
        payload = {
            'username': username,
            'password': password,
            'grant_type': 'password',
            'client_id': self.__client_id,
            'client_secret': self.__client_secret
        }
        params = {'realm-name': self.__realm}
        url = urljoin(self.__url, KeycloakAPIClient.__OPENID_TOKEN).format(**params)
        response = self.__client.post(url=url, payload=payload)
        return response

    def user_introspection(self, user_token):
        """Introspect a user's access token.

        Parameters
        ----------
        user_token : str
            The user's access token to introspect

        Returns
        -------
        Response
            The Response object of the issued request

        """
        params = {'realm-name': self.__realm}
        payload = 'client_id={}&client_secret={}&token={}'\
                  .format(self.__client_id, self.__client_secret, user_token)
        url = urljoin(self.__url, KeycloakAPIClient.__INTROSPECTION).format(**params)
        response = self.__client.post(url=url, headers=self.__headers_form, payload=payload)
        return response
