import logging
import time

from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

import api.swagger.decorators as swagger_decorators
from api import constants
from api.models import Migration
from api.permissions import IsKeycloakAuthenticated
from api.serializers import MigrationSerializer, TransactionSerializer, VContainerSerializer, DatacenterSerializer
from api.tasks import post_vcontainer_to_dcmcm, post_to_dcmcm_for_vpn_creation, post_to_dcmcm_for_migration_rollback
from communication_handler.communication_handler import get_communication_handler

logger = logging.getLogger(constants.API_LOGGER)


class DatacenterRegisterView(CreateAPIView):
    """Datacenter Register View."""
    serializer_class = DatacenterSerializer
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.datacenter_register
    def post(self, request, *args, **kwargs):
        return super(DatacenterRegisterView, self).post(request, *args, **kwargs)


class MigrationCreateView(CreateAPIView):
    """Migration Create View. """
    serializer_class = MigrationSerializer
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.migration_create
    def post(self, request, *args, **kwargs):
        try:
            # Create migration object
            logger.info('[Migration] Received migration from [{}] DC [{}].'
                        .format(request.data['dc_name'], request.data['role']))
            response = super(MigrationCreateView, self).post(request, *args, **kwargs)
            time.sleep(0.2)
            if response.status_code == status.HTTP_201_CREATED:
                logger.info('[Migration] OK. Migration request succeeded.')
                transaction_id = request.data['transaction']
                role = request.data['role']
                # Check existence of both ends of migration
                migration_source = Migration.objects.filter(transaction=transaction_id, role=constants.SOURCE)
                migration_dest = Migration.objects.filter(transaction=transaction_id, role=constants.DESTINATION)
                if (role == constants.SOURCE and migration_dest.count() > 0) \
                        or (role == constants.DESTINATION and migration_source.count() > 0):
                    logger.info('[Migration] Source and destination DC have reported accepted migrations.')
                    # TODO: Fix timing of tasks
                    # create_vpn.delay(transaction_id)
                    # remove_vpn.async_apply(transaction_id, eta=datetime.now() + timedelta())
                    # post_to_dcmcm_for_migration_rollback.async_apply(transaction_id, constants.SOURCE)
                    post_to_dcmcm_for_vpn_creation.delay(transaction_id)
                    post_vcontainer_to_dcmcm.delay(transaction_id)
                else:
                    logger.info('[Migration] Only the [{}] DC has reported accepted migration.'.format(role))
            else:
                logger.warning('[Migration] ERROR. Request failed with response code [{}]'
                               .format(response.status_code))
            return response
        except KeyError:
            logger.warning('[Migration] BAD_REQUEST. Migration request failed')
            logger.debug('[Migration] Payload: `{}`'.format(request.data))
            return Response(status=status.HTTP_400_BAD_REQUEST)


class TokenRetrieveView(APIView):
    """Request Token from Keycloak. """

    @swagger_decorators.get_token
    def post(self, requests, *args, **kwargs):
        try:
            logger.info('[Token] Received request for token.')
            data = requests.data
            response = get_communication_handler().keycloak.get_user_access_token(username=data['username'],
                                                                                  password=data['password'])
            if response.status_code == status.HTTP_200_OK:
                logger.info('[Token] OK. Request was successful.')
                return Response(status=status.HTTP_201_CREATED, data=response.json())
            else:
                logger.warning('[Token] Request failed with status code [{}]'.format(response.status_code))
                return Response(status=response.status_code)
        except KeyError as e:
            logger.warning('[Token] BAD_REQUEST. Invalid of malformed parameters: `{}`.'.format(e))
            return Response(status=status.HTTP_400_BAD_REQUEST)


class TransactionCreateView(CreateAPIView):
    """Transaction Create View. """
    serializer_class = TransactionSerializer
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.update_vc_details
    def post(self, request, *args, **kwargs):
        return super(TransactionCreateView, self).post(request, *args, **kwargs)


class VContainerCreateView(CreateAPIView):
    """Trigger creation of vContainer. """
    serializer_class = VContainerSerializer
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.vcontainer_create
    def post(self, request, *args, **kwargs):
        logger.info('[VC-Create] Received request for vcontainer creation')
        response = super(VContainerCreateView, self).post(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            logger.info('[VC-Create] CREATED. Request succeeded and VContainer object was created')
            return response
        else:
            logger.warning('[VC-Create] Request failed with status code [{}]'.format(response.status_code))
            logger.debug('[VC-Create] Request payload: `{}`, Response Payload: `{}`.'
                         .format(request.data, response.json()))
            return response


class ControllerRetrieveView(APIView):
    """Provide Controller Details. """
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.controller_retrieve
    def get(self, request, *args, **kwargs):
        logger.info('[Controller] Received request for source controller details.')
        try:
            transaction_id = kwargs['transaction_id']
            migration = Migration.objects.get(transaction=transaction_id, role=constants.SOURCE)
            logger.info('[Controller] Forwarding request to source DC of transaction [{}]'.format(transaction_id))
            response = get_communication_handler().dcmcm(migration.dc_name).controller(transaction_id)
            return Response(response.json(), status=status.HTTP_200_OK)
        except Migration.DoesNotExist:
            logger.error('[Controller] No migration was found by this transaction ID')
            return Response(status.HTTP_404_NOT_FOUND)
        except KeyError:
            logger.error('[Controller] ERROR. Bad Request.')
            return Response(status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error('[Controller] A server error occurred: {}'.format(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class RollbackDestinationView(APIView):
    """Rollback Destination View. """
    permission_classes = (IsKeycloakAuthenticated,)

    def get(self, request, *args, **kwargs):
        logger.info('[RollbackDest] Received request for migration rollback at destination')
        try:
            transaction_id = kwargs['transaction_id']
            migration = Migration.objects.get(transaction=transaction_id, role=constants.DESTINATION)
            logger.info('[RollbackDest] Forwarding request to destination DC of transaction [{}]'.format(transaction_id))
            post_to_dcmcm_for_migration_rollback.delay(transaction_id, constants.DESTINATION)
            return Response(status=status.HTTP_202_ACCEPTED)
        except Migration.DoesNotExist:
            logger.error('[RollbackDest] No migration was found by this transaction ID')
            return Response(status.HTTP_404_NOT_FOUND)
        except KeyError:
            logger.error('[RollbackDest] ERROR. Bad Request.')
            return Response(status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error('[RollbackDest] A server error occurred: {}'.format(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
