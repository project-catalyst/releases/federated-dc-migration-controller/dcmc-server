import logging

import time
from rest_framework import status

from api import constants
from api.models import VContainer, Migration, Datacenter
from api.serializers import VContainerSerializer
from communication_handler.communication_handler import get_communication_handler
from dcmc_server.celery import app

logger = logging.getLogger(constants.CELERY_TASK_LOGGER)


@app.task
def post_vcontainer_to_dcmcm(transaction_id):
    """Post VContainer to transaction ID

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction in the IT Load Marketplace

    """
    task_tag = 'task:post_vcontainer_to_dcmcm'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Sending VContainer request to destination DCMC Master'.format(task_tag, transaction_tag))

    # Repeat until VContainer is created
    while True:
        try:
            vcontainer = VContainer.objects.get(transaction=transaction_id)
            break
        except VContainer.DoesNotExist:
            time.sleep(1)
            continue

    # Fetch Destination & Source DC Details
    migration_dest = Migration.objects.get(transaction=transaction_id, role=constants.DESTINATION)
    migration_source = Migration.objects.get(transaction=transaction_id, role=constants.SOURCE)
    source_dc = Datacenter.objects.get(dc_name=migration_source.dc_name)

    # Forward VContainer Details & Source DCMCM URL to Destination DC
    payload = VContainerSerializer(vcontainer).data
    payload['covpn_owner'] = source_dc.username
    response = get_communication_handler().dcmcm(migration_dest.dc_name).vcontainer(payload)
    if response.status_code != status.HTTP_201_CREATED:
        logger.warning('[{}][{}] VContainer request has failed with status code [{}].'
                       .format(task_tag, transaction_tag, response.status_code))
        logger.debug('[{}][{}] Request body: `{}`.'.format(task_tag, transaction_tag, payload))
    else:
        logger.info('[{}][{}] OK. VContainer request was successful.'.format(task_tag, transaction_tag))


@app.task
def post_to_dcmcm_for_vpn_creation(transaction_id):
    """Post to DCMC Master to create VPN for transaction.

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction in the IT Load Marketplace

    """
    task_tag = 'task:post_to_dcmcm_for_vpn_creation'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Sending request for VPN creation to source DCMC Master'
                .format(task_tag, transaction_tag))

    # Fetch Source DC Details
    migration = Migration.objects.get(transaction=transaction_id, role=constants.SOURCE)
    response = get_communication_handler().dcmcm(migration.dc_name).vpn_creation(transaction_id)
    if response.status_code != status.HTTP_202_ACCEPTED:
        logger.warning('[{}][{}] VPN Creation request has failed with status code [{}].'
                       .format(task_tag, transaction_tag, response.status_code))
    else:
        logger.info('[{}][{}] OK. VPN Creation request issued successfully.'.format(task_tag, transaction_tag))


@app.task
def post_to_dcmcm_for_migration_rollback(transaction_id, dc_role):
    """Post to DCMC Master of source DC for migration rollback.

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction in the IT Load Marketplace
    dc_role : {'SOURCE', 'DESTINATION'}
        The role of the DC to rollback

    """
    task_tag = 'task:post_to_dcmcm_for_migration_rollback'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Sending request for migration rollback to [{}] DCMC Master'
                .format(task_tag, transaction_tag, dc_role))
    # Fetch Source DC Details
    migration = Migration.objects.get(transaction=transaction_id, role=dc_role)
    response = get_communication_handler().dcmcm(migration.dc_name).migration_rollback(transaction_id)
    if response.status_code != status.HTTP_202_ACCEPTED:
        logger.warning('[{}][{}] Migration rollback request has failed with status code [{}].'
                       .format(task_tag, transaction_tag, response.status_code))
    else:
        logger.info('[{}][{}] OK. Migration rollback request issued successfully.'.format(task_tag, transaction_tag))
