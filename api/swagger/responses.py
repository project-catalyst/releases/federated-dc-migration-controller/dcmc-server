from rest_framework import status

from api.serializers import ControllerSerializer

Register_POST = {
    status.HTTP_201_CREATED: 'Datacenter was registered.',
    status.HTTP_400_BAD_REQUEST: 'Invalid or malformed data.',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized Datacenter cannot be registered',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user.',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'Only POST requests are allowed to this endpoint.',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

Token_POST = {
    status.HTTP_201_CREATED: 'Access Token was created.',
    status.HTTP_400_BAD_REQUEST: 'Invalid or malformed data.',
    status.HTTP_404_NOT_FOUND: 'The Keycloak Authorization URL was not found. No token was created.',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'Only POST requests are allowed to this endpoint.'
}

Migration_POST = {
    status.HTTP_201_CREATED: 'Reported accepted migration. Migration object was created.',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to create resources',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint.'
}

Transaction_POST = {
    status.HTTP_201_CREATED: 'Updated VC details. Transaction object was created.',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to create resources',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint.'
}

VContainer_POST = {
    status.HTTP_201_CREATED: 'VC Creation was triggered. VC Object was created.',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed access to this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

Controller_GET = {
    status.HTTP_200_OK: ControllerSerializer,
    status.HTTP_400_BAD_REQUEST: 'Invalid of malformed data.',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_404_NOT_FOUND: 'Requested controller information were not found.',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}
