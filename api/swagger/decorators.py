from drf_yasg.utils import swagger_auto_schema

from api.serializers import TokenSerializer, DatacenterSerializer
from api.swagger.parameters import *
from api.swagger.responses import *

# ==================================
#          RETRIEVE TOKEN
# ==================================
get_token = swagger_auto_schema(
    operation_description='Request Token',
    responses=Token_POST,
    request_body=TokenSerializer
)

# ==================================
#       MIGRATION CREATION
# ==================================
migration_create = swagger_auto_schema(
    operation_description='Report accepted migration',
    responses=Migration_POST
)

# ==================================
#           VCONTAINER
# ==================================
update_vc_details = swagger_auto_schema(
    operation_description='Updated VC details',
    responses=Transaction_POST,
    manual_parameters=[TRANSACTION_ID]
)
vcontainer_create = swagger_auto_schema(
    operation_description='Trigger VC Creation',
    responses=VContainer_POST
)

# ==================================
#       DATACENTER REGISTER
# ==================================
datacenter_register = swagger_auto_schema(
    operation_description='Register a Datacenter to the CATALYST Federation',
    responses=Register_POST,
    request_body=DatacenterSerializer
)

# ==================================
#            CONTROLLER
# ==================================
controller_retrieve = swagger_auto_schema(
    operation_summary='Controller details',
    operation_description='Provide CTL Details',
    responses=Controller_GET,
    manual_parameters=[TRANSACTION_ID]
)