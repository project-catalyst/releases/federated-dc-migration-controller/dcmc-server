from drf_yasg import openapi

TRANSACTION_ID = openapi.Parameter(
    name='transaction_id',
    in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER,
    description='The ID of the transaction as kept in CATALYST IT '
                'Load Marketplace and communicated by DCMCL and DCMCM',
    required=True
)
