from django.conf import settings
from django.conf.urls import url
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.urlpatterns import format_suffix_patterns

from api import views

# ==================================
#   SWAGGER ADD-ONS
# ==================================
API_DESCRIPTION = """The API exposed by the DCMC Server component of the H2020 Catalyst Project.

The `swagger-ui` view can be found [here](/catalyst/dcmcs/api/docs).  
The `ReDoc` view can be found [here](/catalyst/dcmcs/api/redoc).  
The swagger `YAML` document can be found [here](/catalyst/dcmcs/api/swagger.yaml).  
"""

schema_view = get_schema_view(
    openapi.Info(
        title='[H2020 CATALYST] DCMC Server API',
        default_version='v1',
        description=API_DESCRIPTION,
        terms_of_service='',
        contact=openapi.Contact(email=''),
        license=openapi.License(name='LGPLv3')
    ),
    url='http://{}:{}'
        .format(settings.DCMCS_HOST['IP'], settings.DCMCS_HOST['PORT']),
    validators=[],
    public=True,
    permission_classes=(permissions.AllowAny,),
)

# ==================================
#   API ENDPOINTS
# ==================================
list_ops = {'get': 'list', 'post': 'create'}
detail_ops = {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}

api_urlpatterns = [
    # API Endpoints
    url(r'^migration/$',
        views.MigrationCreateView.as_view(),
        name='migration-create'),
    url(r'^token/$',
        views.TokenRetrieveView.as_view(),
        name='request-token'),
    url(r'^transaction/(?P<transaction_id>[^/]+)/vcontainer/status/created/$',
        views.TransactionCreateView.as_view(),
        name='update-vc-details'),
    url(r'^transaction/(?P<transaction_id>[^/]+)/controller/$',
        views.ControllerRetrieveView.as_view(),
        name='retrieve-controller-details'),
    url(r'^transaction/(?P<transaction_id>[^/]+)/rollback/destination/$',
        views.RollbackDestinationView.as_view(),
        name='rollback-destination'),
    url(r'^vcontainer/',
        views.VContainerCreateView.as_view(),
        name='vcontainer-create'),
    url(r'^datacenter/register/$',
        views.DatacenterRegisterView.as_view(),
        name='datacenter-register'),

    # Documentation
    url(r'^swagger\.json/$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger\.yaml/$', schema_view.without_ui(cache_timeout=0), name='schema-yaml'),
    url(r'^docs/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

api_urlpatterns = format_suffix_patterns(api_urlpatterns)
