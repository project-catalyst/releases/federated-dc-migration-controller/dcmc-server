from rest_framework import serializers

from api.models import *


class DatacenterSerializer(serializers.ModelSerializer):
    """Datacenter Serializer Class. """

    class Meta:
        model = Datacenter
        fields = ('dc_name', 'ip_address', 'username', 'password')


class MigrationSerializer(serializers.ModelSerializer):
    """Migration Serializer Class. """

    class Meta:
        model = Migration
        fields = ('id', 'dc_name', 'role', 'delivery_start', 'delivery_end', 'transaction')


class TransactionSerializer(serializers.ModelSerializer):
    """Transaction Serializer Class. """

    class Meta:
        model = Transaction
        fields = ('transaction', 'vcontainer')


class VContainerSerializer(serializers.ModelSerializer):
    """VContainer Model Serializer. """

    class Meta:
        model = VContainer
        fields = ('cpu', 'cpu_uom', 'ram', 'ram_uom', 'disk', 'disk_uom', 'start', 'end', 'transaction')


class TokenSerializer(serializers.Serializer):
    """Token Serializer Class. """
    username = serializers.CharField(help_text='Username of issuing DC for gaining authorization in the DCMC Server',
                                     max_length=30)
    password = serializers.CharField(help_text='Password of issuing DC for gaining authorization in the DCMC Server',
                                     max_length=30)


class ControllerSerializer(serializers.Serializer):
    """Controller Response Serializer. """
    name = serializers.CharField(help_text='The name of the OpenStack hosting Keystone in source DC', max_length=30)
    ip = serializers.CharField(help_text='The IP of the OpenStack Node', max_length=15)
