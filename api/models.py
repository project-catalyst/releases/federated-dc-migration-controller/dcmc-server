from django.db import models


class Datacenter(models.Model):
    """Datacenter Model Class.

    Attributes
    ----------
    dc_name : CharField
        The username to register the issuing Datacenter under
    ip_address : URLField
        The URL of the Datacenter's DCMC Master
    username : CharField
        The username used to access the Datacenter's DCMC Master
    password : CharField
        The password used to access the Datacenter's DCMC Master

    """
    dc_name = models.CharField(help_text='The registered name of the Datacenter in the federation',
                               max_length=30, unique=True)
    ip_address = models.URLField(help_text='The IP Address of the Datacenter under registration')
    username = models.CharField(help_text='The username for accessing the Datacenter\'s DCMC Master', max_length=30)
    password = models.CharField(help_text='The password for accessing the Datacenter\'s DCMC Master', max_length=30)

    class Meta:
        order_with_respect_to = 'dc_name'

    def __str__(self):
        return 'DC_NAME: %s | IP_ADDRESS: %s' % (self.dc_name, self.ip_address)


class Migration(models.Model):
    """Migration Model Class.

    Attributes
    ----------
    dc_name : CharField
        Username of the issuing DC as understood by the DCMC Server
    role : CharField
        The role of the issuing DC in the migration ("source" or "destination")
    delivery_start : DateTimeField
        The time on which the migration is planned to start
    delivery_end : DateTimeField
        The time on which the remote execution is planned to end
    transaction : IntegerField
        The ID of the transaction as kept in the CATALYST IT Load Marketplace

    """
    MIGRATION_ROLES = (
        ('source', 'Source DC in Migration'),
        ('destination', 'Destination DC in Migration')
    )

    dc_name = models.CharField(help_text='Username of the issuing DC as understood by the DCMC Server', max_length=30)
    role = models.CharField(help_text='The role of the issuing DC in the migration ("source" or "destination")',
                            choices=MIGRATION_ROLES, max_length=12)
    delivery_start = models.DateTimeField(help_text='The time on which the migration is planned to start')
    delivery_end = models.DateTimeField(help_text='The time on which the remote execution is planned to end')
    transaction = models.IntegerField(help_text='The ID of the transaction as kept in the CATALYST IT Load Marketplace')

    class Meta:
        order_with_respect_to = 'transaction'

    def __str__(self):
        return 'TRANSACTION: %s | DC_NAME: %s | ROLE: %s | DELIVERY_START: %s | DELIVERY_END: %s' % \
               (self.transaction, self.dc_name, self.role, self.delivery_start, self.delivery_end)


class Transaction(models.Model):
    """Transaction Model Class.

    Attributes
    ----------
    transaction : IntegerField
        The ID of the transaction as kept in the CATALYST IT Load Marketplace
    vcontainer : CharField
        Identification info of the issuing VM (UUID)

    """
    transaction = models.IntegerField(help_text='The ID of the transaction as kept in the CATALYST IT Load Marketplace')
    vcontainer = models.CharField(help_text='Identification info of the issuing VM (UUID)', max_length=100)

    class Meta:
        order_with_respect_to = 'transaction'

    def __str__(self):
        return 'TRANSACTION: %s | VCONTAINER_UUID: %s' % (self.transaction, self.vcontainer)


class VContainer(models.Model):
    """ vContainer Model Class.

    Attributes
    ----------
    cpu : IntegerField
        The number of CPU cores of the VC described
    cpu_uom : CharField
        The unit of measurement for the CPU value
    ram : IntegerField
        The amount of RAM of the VC described
    ram_uom : CharField
        The unit of measurement for the RAM value
    disk : IntegerField
        The amount of disk of the VC described
    disk_uom : CharField
        The unit of measurement for the disk value
    start : DateTimeField
        The time from which the VC is planned to be active
    end : DateTimeField
        The time until which the VC is planned to be active
    transaction : IntegerField
        The ID of the transaction as kept in the CATALYST IT Load Marketplace

    """
    VC_STATUSES = (
        ('created', 'VC has been created'),
        ('registered', 'VC has been registered')
    )

    cpu = models.IntegerField(help_text='The number of CPU cores of the VC described')
    cpu_uom = models.CharField(help_text='The unit of measurement for the CPU value', max_length=5)
    ram = models.IntegerField(help_text='The amount of RAM of the VC described')
    ram_uom = models.CharField(help_text='The unit of measurement for the RAM value', max_length=5)
    disk = models.IntegerField(help_text='The amount of disk of the VC described')
    disk_uom = models.CharField(help_text='The unit of measurement for the disk value', max_length=5)
    start = models.DateTimeField(help_text='The time from which the VC is planned to be active')
    end = models.DateTimeField(help_text='The time until which the VC is planned to be active')
    transaction = models.IntegerField(help_text='The ID of the transaction as kept in the CATALYST IT Load Marketplace')

    class Meta:
        order_with_respect_to = 'transaction'

    def __str__(self):
        return 'TRANSACTION: %s | FLAVOR: %s-%s-%s | START: %s | END: %s' % \
               (self.transaction, self.cpu, self.ram, self.disk, self.start, self.end)
