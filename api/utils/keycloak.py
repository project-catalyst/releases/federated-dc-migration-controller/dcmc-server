from rest_framework import status

from communication_handler.communication_handler import get_communication_handler

keycloak_client = None


def check_user_validity(user_token):
    """Checks Keycloak User Validity.

    Parameters
    ----------
    user_token : str
        A Keycloak User Auth Token

    Returns
    -------
    The corresponding username of the token, else `inactive`

    """
    response = get_communication_handler().keycloak.user_introspection(user_token)
    if response.status_code == status.HTTP_200_OK:
        introspected = response.json()
        if introspected != {'active': False}:
            return introspected['username']
        return 'inactive'
    return 'inactive'
