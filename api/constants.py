# Migration roles
SOURCE = 'source'
DESTINATION = 'destination'

# VC Statuses
CREATED = 'created'
REGISTERED = 'registered'

# Loggers
API_LOGGER = 'api_logger'
CELERY_TASK_LOGGER = 'task_logger'
