#!/usr/bin/env bash

# Wait for postgres
while ! nc -z dcmcs_postgres 5432; do
  echo "Waiting for postgres server..."
  sleep 1
done

# Make migrations
cd /opt/dcmc-server
python3 manage.py migrate --settings=dcmc_server.settings.${1}

# Supervisor
supervisord -c /etc/supervisor/supervisord.conf
update-rc.d supervisor defaults

# Start gunicorn
gunicorn dcmc_server.wsgi:application

echo "Initialization completed."
tail -f /dev/null  # Necessary in order for the container to not stop
