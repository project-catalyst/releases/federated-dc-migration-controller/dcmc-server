.. DCMC Server documentation master file, created by
   sphinx-quickstart on Tue Oct  1 10:21:10 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#########################
DCMC Server Documentation
#########################
This is the documentation for the code implementing the Server of the DC Migration Controller (DCMC) of the H2020
Catalyst Project. The DCMC Server lies in the Federation part of the CATALYST architecture and keeps track of the
DCs in the federation, while being responsible for triggering the initiation and synchronization of the migration
procedures.

For more details about the role of the DCMC Master in the overall DC Migration Controller, please refer to H2020
CATALYST D3.3 *Federated DCs Migration Controller* deliverable report.

.. toctree::
   :maxdepth: 3
   :caption: Table of Contents:


*************************
Installation & Deployment
*************************
In the following, the essentials for the installation of a DCMC Server instance are described, including
prerequisites for the deployment, the configuration of the services and the actual deployment.

.. toctree::
   :maxdepth: 2

Prerequisites
=============
For the deployment of the DCMC Server component the Docker engine as well as docker-compose should be installed.
These actions can be performed following the instructions provided below. Firstly, an update should be performed
and essential packages should be installed::

   sudo apt-get update
   sudo apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common


Secondly the key and Docker repository should be added::

   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
   sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"

Then another update is performed, Docker is installed and the user is added to docker group::

   sudo apt-get update
   sudo apt-get install -y docker-ce
   sudo groupadd docker
   sudo usermod -aG docker $USER

Finally, docker-compose should be installed::

   sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   sudo chmod +x /usr/local/bin/docker-compose

For more information about Docker and docker-compose, please check at https://docs.docker.com/.

Environmental Parameters
========================
The services that are executed as containers, thus forming the DCMC Server receive their configurations
from a .env file located in the root of the repository. In the following tables, the environmental parameters
that are necessary for the configuration and deployment of the DCMC Server are recorded and described.

|

.. list-table:: Docker and Internal Services
   :widths: 35 30 35
   :header-rows: 1

   * - Parameter
     - Tag
     - Description
   * - COMPOSE_PROJECT_NAME
     - `docker`
     - The name of the project (default: catalyst)
   * - PG_IMAGE_TAG
     - `PostgreSQL`
     - The Postgresql Docker Image tag
   * - PG_PORT
     - `PostgreSQL`
     - Port
   * - PG_USER
     - `PostgreSQL`
     - User
   * - PG_PASSWORD
     - `PostgreSQL`
     - Password
   * - PG_DB
     - `PostgreSQL`
     - DB Name
   * - REDIS_IMAGE_TAG
     - `Redis`
     - Docker Image tag
   * - REDIS_PORT
     - `Redis`
     - Port
   * - DCMCS_ENV
     - `DCMC Server`
     - Environment (dev / prod)
   * - DCMCS_API_PORT
     - `DCMC Server`
     - API Port
   * - DCMCS_SUPERVISOR_PORT
     - `DCMC Server`
     - Supervisor Port
   * - DCMCS_DB_HOST
     - `DCMC Server`
     - DB Host
   * - DCMCS_DB_PORT
     - `DCMC Server`
     - DB Port
   * - DCMCS_DB_USER
     - `DCMC Server`
     - DB User
   * - DCMCS_DB_PASSWORD
     - `DCMC Server`
     - DB Password
   * - DCMCS_DB_NAME
     - `DCMC Server`
     - DB Name
   * - DCMCS_REDIS_HOST
     - `DCMC Server`
     - Redis Host
   * - DCMCS_REDIS_PORT
     - `DCMC Server`
     - Redis Port
   * - DCMCS_HOST_IP
     - `DCMC Server`
     - Host IP
   * - DCMCS_HOST_PORT
     - `DCMC Server`
     - Port
   * - DCMCS_HOST_PROTOCOL
     - `DCMC Server`
     - Protocol

|

.. list-table:: External Services
   :widths: 35 30 35
   :header-rows: 1

   * - Parameter
     - Tag
     - Description
   * - DCMCS_KEYCLOAK_URL
     - `Keycloak`
     - Host URL
   * - DCMCS_KEYCLOAK_REALM
     - `Keycloak`
     - Realm
   * - DCMCS_KEYCLOAK_CLIENT_ID
     - `Keycloak`
     - Client ID
   * - DCMCS_KEYCLOAK_CLIENT_SECRET
     - `Keycloak`
     - Client Secret

|


Deployment
==========
The DCMC Server is deployed as collection of Docker containers, utilizing docker-compose. Having cloned the
code of this repository, and having created the .env file the following commands should be executed::

   cp .env /dcmc-server
   cd dcmc-server
   docker-compose up --build -d


|

***************
DCMC Server API
***************
This is the documentation of the API offered by the DCMC Server component. This documentation is composed by:

  * The API Endpoints;
  * The Django Models;
  * Asynchronous (Celery) Tasks;
  * Random essential utils;

For information about the interaction with other components and their respective implemented API clients please
refer to the next section of this document. For a detailed overview of the available requests offered byt the API
you may refer to the available Swagger documentation, after deployment, available at http://<HOST_IP>:<HOST_PORT>/catalyst/dcmcs/api/docs.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


API Endpoints
=============
This section briefly documents the API offered by the DCMC Server.

.. openapi:: dcmcs.yml


Federation DB & Django Models
=============================
The Federation DB, lying on the DCMC Server component of the federation, includes, essentially the following tables:

  * **Migration**: Reports migrations accepted from DCs;
  * **VContainer**: Representation of the VM to be created during the migration
  * **Transaction**: Keeps information about the transaction and VM uuid

The respective Django models for the aforementioned DB tables are documented in this section.

.. automodule:: api.models
   :members:

|

Asynchronous Tasks
==================

.. autotask:: api.tasks.post_vcontainer_to_dcmcm
.. autotask:: api.tasks.post_to_dcmcm_for_vpn_creation
.. autotask:: api.tasks.post_to_dcmcm_for_migration_rollback

|

Random Essential Utils
======================
.. automodule:: api.utils.keycloak
   :members:

|

***********************
DCMC Server API Wrapper
***********************
This part of the documentation refers to the available DCMC Server API Wrapper. The wrapper can bu utilized by another
projects "as is" or in a reduced version, including only the essential API calls.

.. automodule:: dcmc_server_api_wrapper.dcmcs_api
   :members:

|

*********************
Communication Handler
*********************
In this section, the documentation of the API Clients that are utilized by the DCMC Server is provided.
The DCMCS utilizes these client through the Communication Handler Class. All the external interactions of DCMCS are made
through this class. The DCMCS interacts with:

  * The DCMC Master Clients of both ends (source and destination);
  * Keycloak.

In the context of the DCMCS, the API clients include methods performing operations as needed by the present component only.

.. automodule:: communication_handler.communication_handler
   :members:

|

.. toctree::
   :maxdepth: 2

DCMC Master Client
==================
.. automodule:: communication_handler.api_clients.dcmcm_api
   :members:

|

Keycloak API Client
===================
.. automodule:: communication_handler.api_clients.keycloak_api
   :members:

|

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
