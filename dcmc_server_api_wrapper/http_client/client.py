import json

import requests
import urllib3
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

from .baseclient import AbstractClient

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class Client(AbstractClient):
    """Client Class.

    This is a HTTP Client Class, supporting all kinds of HTTP requests. Request
    retrial is also supported on failed requests (500, 502, 503, 504).

    Attributes
    ----------
    verify_ssl_sert : bool
        Denotes whether SSL Certificate should be verified.

    Methods
    -------
    request_with_retrial(retries, session, backoff_factor, status_forcelist)
        Performs HTTP(S) request with retrial in case an error status code is returned
    list(url, headers, **kwargs)
        Fetch a list (collection) of entities.
    get(url, headers, **kwargs)
        Fetch a specific entity.
    post(url, headers, payload, **kwargs)
        Post a payload to an endpoint to possibly create an entity.
    delete(url, headers, **kwargs)
        Delete an entity.

    """

    MAX_RETRIES = 5
    RETRY_STATUSES = (500, 502, 503, 504)

    def __init__(self, verify_ssl_cert=False):
        """Client Class Constructor.

        Parameters
        ----------
        verify_ssl_cert : bool
            Denotes whether SSL Certificate should be verified

        """
        self.verify_ssl_cert = verify_ssl_cert
        super(Client, self).__init__()

    @staticmethod
    def request_with_retrial(retries, session=None, backoff_factor=0.3, status_forcelist=(500, 502, 503, 504)):
        session = session or requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
            status_forcelist=status_forcelist,
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session

    def list(self, url, headers=None, **kwargs):
        """Fetch a list of entities (a collection).

        Parameters
        ----------
        url : str
            The endpoint of the web service
        headers : dict
            The required HTTP headers, e.g., Accept: application/json
        kwargs : dict, optional
            Additional arguments to be passed to the request.

        Returns
        -------
        response : Response
            A Response Object

        """
        if headers is None:
            headers = {}
        query_params = kwargs.get('query_params', None)
        request = self.request_with_retrial(retries=Client.MAX_RETRIES, status_forcelist=Client.RETRY_STATUSES)
        response = request.get(url, headers=headers, params=query_params, verify=self.verify_ssl_cert)
        return response

    def get(self, url, headers=None, **kwargs):
        """Fetch an entity.

        Parameters
        ----------
        url : str
            The endpoint of the web service
        headers : dict
            The required HTTP headers, e.g., Accept: application/json
        kwargs : dict, optional
            Additional arguments to be passed to the request.

        Returns
        -------
        response : Response
            A Response Object

        """
        if headers is None:
            headers = {}
        query_params = kwargs.get('query_params', None)
        request = self.request_with_retrial(retries=Client.MAX_RETRIES, status_forcelist=Client.RETRY_STATUSES)
        response = request.get(url, headers=headers, params=query_params, verify=self.verify_ssl_cert)
        return response

    def post(self, url, headers=None, payload=None, **kwargs):
        """Create an entity or simply post data.

        Parameters
        ----------
        url : str
            The endpoint of the web service
        headers : dict
            The required HTTP headers, e.g., Accept: application/json
        payload : dict
            Data that will be encoded as JSON and passed in the request
        kwargs : dict, optional
            Additional arguments to be passed to the request.

        Returns
        -------
        response : Response
            A Response object

        """
        if headers is None:
            headers = {}
        query_params = kwargs.get('query_params', None)
        request = self.request_with_retrial(retries=Client.MAX_RETRIES, status_forcelist=Client.RETRY_STATUSES)
        response = request.post(url, data=json.dumps(payload), headers=headers,
                                params=query_params, verify=self.verify_ssl_cert)
        return response

    def delete(self, url, headers=None, **kwargs):
        """Delete an entity.

        Parameters
        ----------
        url : str
            The endpoint of the web service
        headers : dict
            The required HTTP headers, e.g., Accept: application/json
        kwargs : dict, optional
            Additional arguments to be passed to the request.

        Returns
        -------
        response : Response
            A Response Object

        """
        if headers is None:
            headers = {}
        query_params = kwargs.get('query_params', None)
        request = self.request_with_retrial(retries=Client.MAX_RETRIES, status_forcelist=Client.RETRY_STATUSES)
        response = request.delete(url=url, headers=headers, params=query_params, verify=self.verify_ssl_cert)
        return response
