from http import HTTPStatus
from logging import getLogger
from urllib.parse import urljoin

from dcmc_server_api_wrapper.http_client.client import Client


class DCMCServerClient(object):
    """DCMC Server Client Class.

    This class serves as a wrapper for the API endpoints exposed by the DCMC Server (DCMCS) component of the
    H2020 CATALYST Migration Controller. It covers calls to the entire available API of the DCMC Server API.

    Methods
    -------
    controller(transaction_id, tag)
        Retrieve the Controller details of Source
    datacenter_register(dc_name, ip_address, username, password)
        Register Datacenter to DCMC Server of Federation
    migration(dc_name, role, delivery_start, delivery_end, transaction, tag)
        Report accepted migration to DCMC Server
    rollback_destination(transaction_id)
        Send rollback request to Destination DC
    token(username, password)
        Request token from DCMC Server
    vcontainer(cpu, cpu_uom, ram, ram_uom, disk, disk_uom, start, end, transaction, tag)
        Send vcontainer details for creation
    vcontainer_created(transaction_id, tag)
        Inform DCMC Server of VContainer creation

    Examples
    --------
    `Instantiate a DCMC Server Client`

    >>> from dcmc_server_api_wrapper.dcmcs_api import DCMCServerClient
    >>> dcmcs = DCMCServerClient(dcmcs_host_url=DCMCS_HOST_URL, username=USERNAME, password=PASSWORD)

    `Controller details`

    >>> dcmcs.controller(transaction_id=TRANSACTION_ID)

    `Datacenter Register`

    >>> dcmcs.datacenter_register(dc_name=DC_NAME, ip_address=IP_ADDRESS, username=USERNAME, password=PASSWORD)

    `Migration`

    >>> dcmcs.migration(dc_name=DC_NAME, role=ROLE, delivery_start=START, delivery_end=END, transaction=TRANSACTION_ID)

    `Rollback Destination`

    >>> dcmcs.rollback_destination(transaction_id=TRANSACTION_ID)

    `Token`

    >>> dcmcs.token(username=USERNAME, password=PASSWORD)

    `VContainer`

    >>> dcmcs.vcontainer(cpu=CPU, cpu_uom=CPU_UOM, ram=RAM, ram_uom=RAM_UOM,
    ...                  disk=DISK, disk_uom=DISK_UOM, start=START, end=END,
    ...                  transaction=TRANSACTION)

    `VContainer Registered`

    >>> dcmcs.vcontainer_created(transaction_id=TRANSACTION_ID)


    """
    __API_PREFIX = 'catalyst/dcmcs/api/'
    __DATACENTER_REGISTER = 'datacenter/register/'
    __MIGRATION = 'migration/'
    __TOKEN = 'token/'
    __VCONTAINER = 'vcontainer/'
    __CONTROLLER_DETAILS = 'transaction/{transaction_id}/controller/'
    __VCONTAINER_CREATED = 'transaction/{transaction_id}/vcontainer/status/created/'
    __ROLLBACK_DESTINATION = 'transaction/{transaction_id}/rollback/destination/'

    def __init__(self, dcmcs_host_url, username, password):
        """DCMC Server Client Class Constructor.

        Parameters
        ----------
        dcmcs_host_url : str
            The host URL of the DCMC Server.
        username : str
            The username of the DCMC Master registered in the DCMC Server
        password : str
            The password of the DCMC Master registered in the DCMC Server

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = urljoin(dcmcs_host_url, DCMCServerClient.__API_PREFIX)
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        self.log = getLogger()
        self.__token = self.token(username, password)
        self.__headers['Authorization'] = 'Bearer {}'.format(self.__token)

    def token(self, username, password):
        """Request token from DCMC Server.

        Parameters
        ----------
        username : str
            The username of the issuing DC for gaining authorization in the DCMC Server.
        password : str
            The password of the issuing DC for gaining authorization in the DCMC Server.

        Returns
        -------
        token : str
            A Keycloak access token

        Examples
        --------
        >>> from dcmc_server_api_wrapper.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(dcmcs_host_url=DCMCS_HOST_URL, username=USERNAME, password=PASSWORD)
        >>> dcmcs.token(username=USERNAME, password=PASSWORD)

        """
        payload = {
            'username': username,
            'password': password
        }
        url = urljoin(self.__url, DCMCServerClient.__TOKEN)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.CREATED.value:
            self.log.error('Could not get access token.')
            self.log.debug('Response payload: `{}`'.format(response.text))
            return None
        self.log.info('Successfully retrieved access token.')
        return response.json()['access_token']

    def controller(self, transaction_id, tag=''):
        """Fetch the Controller details.

        This endpoint is intended to be used by the DCMC Lite Client deployed in the vCMP of the Destination
        to retrieve details from the Controller it is supposed to subscribe to (Source). Until this request
        succeeds, the DCMC Lite Client is now aware of these details.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction to fetch controller details for
        tag : str
            A logging tag

        Returns
        -------
        dict, None
            The response payload if request succeeds, else `None`

        Examples
        --------
        >>> from dcmc_server_api_wrapper.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(dcmcs_host_url=DCMCS_HOST_URL, username=USERNAME, password=PASSWORD)
        >>> dcmcs.controller(transaction_id=TRANSACTION_ID)
        {
            'name': 'DC1',
            'ip': '192.168.255.6',
            'port': 60011
        }

        """
        self.log.info('{} Retrieving controller details ...'.format(tag))
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCServerClient.__CONTROLLER_DETAILS).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('{} Request failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return None
        self.log.info('{} OK. Successfully retrieved controller details'.format(tag))
        return response.json()

    def migration(self, dc_name, role, delivery_start, delivery_end, transaction, tag=''):
        """Report accepted migration to DCMC Server.

        The migration endpoint is used by the DCMC Master Clients of both the Source and the Destination DCs
        to report an accepted migration bid or offer (according to the IT Load Marketplace) to the DCMC Server.
        When both of the participating DCs report their readiness to the DCMC Server, the migration commences.

        Parameters
        ----------
        dc_name : str
            The username of the issuing DC as understood by the DCMC Server.
        role : {'source', 'destination'}
            The role of the issuing DC in the migration.
        delivery_start : str
            The time on which the migration is planned to start.
        delivery_end : str
            The time on which the migration is planned to end.
        transaction : int
            The ID of transaction as kept in the IT Load Marketplace
        tag : str, optional
            The logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_server_api_wrapper.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(dcmcs_host_url=DCMCS_HOST_URL, username=USERNAME, password=PASSWORD)
        >>> dcmcs.migration(dc_name='DC1', role='destination',
        ...                 delivery_start='2019-10-01T21:00:00Z',
        ...                 delivery_end='2019-10-01T22:00:00Z', transaction=90008)
        True

        """
        self.log.info('{} Sending migration request to DCMC Server'.format(tag))
        payload = {
            'dc_name': dc_name,
            'role': role,
            'delivery_start': delivery_start,
            'delivery_end': delivery_end,
            'transaction': transaction
        }
        url = urljoin(self.__url, DCMCServerClient.__MIGRATION)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.CREATED.value:
            self.log.error('{} Migration request failed with status code [{}].'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} OK. Migration request was successful.'.format(tag))
        return True

    def vcontainer(self, cpu, cpu_uom, ram, ram_uom, disk, disk_uom, start, end, transaction, tag=''):
        """Send VContainer details to the DCMC Server.

        This service is meant to be used by the DCMC Master Client of the source DC in order to sent the VContainer
        details to the DCMC Server. The DCMC Server will then forward these details to the DCMC Master Client of the
        destination DC and the creation of the VContainer will be triggered.

        Parameters
        ----------
        cpu : int
            The number of CPU cores of the VC described
        cpu_uom : str
            The unit of measurement for the CPU value
        ram : int
            The amount of RAM of the VC described
        ram_uom : str
            The unit of measurement of the Ram value
        disk: int
            The amount of disk of the VC described
        disk_uom : str
            The unit of measurement of the VC described
        start : str
            The time from which the VC is planned to be active
        end : str
            The time until which the VC is planned to be active
        transaction : int
            The transaction ID as kept in the IT Load Marketplace
        tag : str, optional
            The logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_server_api_wrapper.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(dcmcs_host_url=DCMCS_HOST_URL, username=USERNAME, password=PASSWORD)
        >>> dcmcs.vcontainer(cpu=1, cpu_uom='cpu', ram=2048, ram_uom='MB', disk=20,
        ...                  disk_uom='GB', start='2019-10-01T21:00:00Z',
        ...                  end='2019-10-01T22:00:00Z', transaction=90099)
        True

        """
        self.log.info('{} Sending VContainer request to DCMC Server'.format(tag))
        payload = {
            'cpu': cpu,
            'cpu_uom': cpu_uom,
            'ram': ram,
            'ram_uom': ram_uom,
            'disk': disk,
            'disk_uom': disk_uom,
            'start': start,
            'end': end,
            'transaction': transaction
        }
        url = urljoin(self.__url, DCMCServerClient.__VCONTAINER)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.CREATED.value:
            self.log.error('{} VContainer request failed with status code [{}].'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} OK. VContainer request was successful.'.format(tag))
        return True

    def vcontainer_created(self, transaction_id, tag=''):
        """Notify DCMC Server of VContainer creation.

        This service is meant to be used by the DCMC Lite Client in order to inform the DCMC Server that
        is has been created for a certain transaction and is ready to accept a migrated load.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction in the Catalyst IT Load Marketplace
        tag : str, optional
            The logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_server_api_wrapper.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(dcmcs_host_url=DCMCS_HOST_URL, username=USERNAME, password=PASSWORD)
        >>> dcmcs.vcontainer_created(transaction_id=TRANSACTION_ID)
        True

        """
        self.log.info('{} Informing DCMC Server of VContainer Creation'.format(tag))
        params = {'transaction_id': transaction_id}
        # TODO: Remove dummy payload
        payload = {'transaction': transaction_id, 'vcontainer': 'uuid'}
        url = urljoin(self.__url, DCMCServerClient.__VCONTAINER_CREATED).format(**params)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.CREATED.value:
            self.log.error('{} Request failed with response code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} OK. Request to DCMC Server succeeded'.format(tag))
        return True

    def datacenter_register(self, dc_name, ip_address, username, password, tag=''):
        """Register Datacenter to the DCMC Server of the federation.

        This endpoint is meant to be used by each of the Datacenters of the CATALYST Federation. Each DCMC Master
        is registered with a Keycloak username and password beforehand. When the DCMC Master is deployed, it will
        send a registration request to the DCMC Server, serving as a confirmation and gives the DCMC Server details
        of the deployment (e.g. DCMC Master API URL).

        Parameters
        ----------
        dc_name : str
            The unique name of the DC to register
        ip_address : URL
            The host URL of the DCMC Master
        username : str
            The Keycloak Username of the DCMC Master
        password : str
            The Keycloak password of the DCMC Master
        tag : str, optional
            The logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_server_api_wrapper.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(dcmcs_host_url=DCMCS_HOST_URL, username=USERNAME, password=PASSWORD)
        >>> dcmcs.datacenter_register(dc_name=DC_NAME, ip_address=IP_ADDRESS, username=USERNAME, password=PASSWORD)
        True

        """
        self.log.info('{} Registering Datacenter to DCMC Server'.format(tag))
        payload = {
            'dc_name': dc_name,
            'ip_address': ip_address,
            'username': username,
            'password': password
        }
        url = urljoin(self.__url, DCMCServerClient.__DATACENTER_REGISTER)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.CREATED.value:
            self.log.error('{} Registration has failed with status code [{}].'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} Registration to DCMC Server was successful'.format(tag))
        return True

    def rollback_destination(self, transaction_id, tag=''):
        """Inform server for migration rollback at destination

        This endpoint is meant to be used by the DCMC Master of the Source DC. When the IT Load exchange reaches an
        end, the IT Load should rollback to its original DC, and complementary tasks should be performed as well.
        When the Source side finishes with the rollback, it will send a rollback request to the Destination side
        through DCMC Server in order for it to delete the deployed vCMP.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction to rollback migration for
        tag : str, optional
            The logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_server_api_wrapper.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(dcmcs_host_url=DCMCS_HOST_URL, username=USERNAME, password=PASSWORD)
        >>> dcmcs.rollback_destination(transaction_id=TRANSACTION_ID)
        True

        """
        self.log.info('{} Sending rollback request to destination'.format(tag))
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCServerClient.__ROLLBACK_DESTINATION).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code != HTTPStatus.ACCEPTED.value:
            self.log.error('{} Rollback request failed with status code [{}].'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} Rollback destination request was accepted by DCMC Server.'.format(tag))
        return True
